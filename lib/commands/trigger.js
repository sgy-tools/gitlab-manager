'use strict';

const gitHelper = require('../../services/gitHelper');
const gitlabService = require('../../services/gitlabService');

exports.command = 'trigger <access-token>';

exports.aliases = ['tb'];

exports.desc = 'Trigger build.';

exports.handler = (args) => {

    let accessToken = args.accessToken;

    let branch = gitHelper.getCurrentBranch();
    let remoteOriginUrl = gitHelper.getRemoteOriginUrl();

    gitlabService.getProjectFromRemoteUrl(accessToken, remoteOriginUrl)
    .then(project => {
        gitlabService.getTriggersForProject(accessToken, project)
        .then(gitlabService.getTokenOfTrigger)
        .then(token => {
            console.log('Branch: ' + branch.replace('refs/heads/', ''));
            console.log('Token: ' + token);

            gitlabService.triggerBuild(accessToken, project.id, token, branch)
            .then((result) => {
                console.log(result);
            });

        });

    });
};

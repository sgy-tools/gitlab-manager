#!/usr/bin/env node

'use strict';

const { join } = require('path');
const yargs = require('yargs');

yargs
    .usage('\nGitLab Manager\nUsage: glm <command> [options]')
    .commandDir(join(__dirname, 'lib', 'commands'))
    .demand(1)
    .describe('v', 'Verbose')
    .alias('v', 'verbose')
    .help('h')
    .describe('h', 'Show help')
    .alias('h', 'help')
    .argv;

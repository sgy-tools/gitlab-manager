'use strict';

const shell = require('shelljs');

let gitHelper = {
    getCurrentBranch: getCurrentBranch,
    getRemoteOriginUrl: getRemoteOriginUrl,
    isGitRepositoryHere: isGitRepositoryHere
};

module.exports = gitHelper;

/* Private functions */

function getCurrentBranch() {
    let branch = shell.exec('git symbolic-ref HEAD', { silent:true }).stdout;
    branch = branch.trim();

    return branch;
}

function getRemoteOriginUrl() {
    let remoteOriginUrl = shell.exec('git config --get remote.origin.url', {silent:true}).stdout.trim();

    if(!remoteOriginUrl.endsWith('.git')) {
        remoteOriginUrl = remoteOriginUrl + '.git';
    }

    return remoteOriginUrl;
}

function isGitRepositoryHere() {
    shell.exec('git status', { async: false }, (code, stdout, stderr) => {
        return code === 0;
    });
}

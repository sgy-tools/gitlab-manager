'use strict';

const request = require('request');
const config = require('../conf/config.json');

let apiBaseUrl = config.gitlab.url;

const gitlabService = {
    getJSONData: getJSONData,
    post: post,
    accessToken: undefined
};

module.exports = gitlabService;

/* Private fonctions */

function getJSONData(accessToken, resource) {

    let url = `${apiBaseUrl}${resource}`;
    console.log(`Request to: ${resource}`);

    return new Promise((resolve, reject) => {
        request.get({
            url: url,
            "rejectUnauthorized": false, // pas sécur
            headers: {
                'PRIVATE-TOKEN': accessToken,
                'Content-Type': 'application/json'
            }
        })
        .on('response', (response) => {
            if (response.statusCode >= 200 && response.statusCode < 300) {
                response.on('data', (data) => {
                    resolve(JSON.parse(data));
                });
            } else {
                reject(response.statusCode + ' ' + response.statusMessage);
            }
        })
        .on('error', reject);
    });
}

function post(accessToken, resource, formValues) {

    console.log(`Request to: ${resource}`);

    return new Promise((resolve, reject) => {
        request.post({
            url: `${apiBaseUrl}${resource}`,
            form: formValues,
            "rejectUnauthorized": false,
            headers: {
                'PRIVATE-TOKEN': accessToken
            }
        }, function(err, httpResponse, body) {
            if (err) {
                reject(err);
                return;
            }

            resolve(JSON.parse(body));
        });
    });

}

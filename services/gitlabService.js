'use strict';

const requestService = require('./requestService');

const gitlabService = {
    getProjectFromRemoteUrl: getProjectFromRemoteUrl,
    getTriggersForProject: getTriggersForProject,
    getTokenOfTrigger: getTokenOfTrigger,
    triggerBuild: triggerBuild
};

module.exports = gitlabService;

/* Private fonctions */

function getProjectFromRemoteUrl(accessToken, remoteOriginUrl) {
    return new Promise((resolve, reject) => {
        let resource = `/projects`;

        requestService.getJSONData(accessToken, resource)
        .then(projects => {
            if (Array.isArray(projects) && projects.length) {
                projects = projects
                    .filter(p => p.ssh_url_to_repo === remoteOriginUrl);

                if (Array.isArray(projects) && projects.length) {
                    console.log('Project: ' + projects[0].name);

                    resolve(projects[0]);
                } else {
                    reject('Project not found with remote URL.');
                }
            } else {
                reject('Projects not loaded properly.');
            }
        })
        .catch(reject);
    });
}

function getTriggersForProject(accessToken, project) {
    return new Promise((resolve, reject) => {
        let resource = `/projects/${ project.id }/triggers`;

        requestService.getJSONData(accessToken, resource)
        .then(resolve)
        .catch(reject);
    });
}

function getTokenOfTrigger(triggers) {
    return new Promise((resolve, reject) => {
        if (Array.isArray(triggers) && triggers.length) {
            resolve(triggers[0].token);
        } else {
            reject('No valid token found.');
        }
    });
}

function triggerBuild(accessToken, projectId, token, branch) {
    return new Promise((resolve, reject) => {
        let resource = `/projects/${projectId}/trigger/builds`
        requestService.post(accessToken, resource, {
            token: token,
            ref: branch
        })
        .then(body => {
            resolve(`Build started.  Trigger id: ${body.id}`);
        })
        .catch(reject);
    });
}
